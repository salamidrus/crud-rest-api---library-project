var express = require('express');
var bodyParser = require('body-parser');

var app = express();
// require('dotenv').config();
// console.log()
var book = require('./routes/product');
const PORT =  2424;

// var db = 'mongodb://localhost/books';
// mongoose.connect(db, { useNewUrlParser: true });
var mongoose = require('mongoose');
mongoose.connect('mongodb+srv://idrus:idrus123@cluster0-5hgne.mongodb.net/libraryBooks?retryWrites=true', { useNewUrlParser: true });

// app.use(bodyParser.json())
// app.use(bodyParser.urlencoded( {
//     extended: true
// }));
app.use(express.json);
app.use(express.urlencoded);
app.use('/', book);

app.listen(PORT, function() {
  console.log("app listen on port " +PORT);
});

