var Book = require('../models/product');


exports.check = function(req, res) {
    res.send("Happy to be here");
};

exports.showBooks = function(req, res) {
    console.log('getting all books');
    Book.find({})
      .exec(function(err, books) {
        if(err) {
          res.send('error occured')
        } else {
          console.log(books);
          res.json(books);
        }
      });
};

exports.showBooksId = function(req, res){
  console.log("geeting one book")
  Book.findOne({
      _id: req.params.id
  })
  .exec(function(err, books) {
      if(err) {
          res.send('error occured')
      } else {
          console.log(books);
          res.json(books);
      }
  })
}

// exports.addBooks = function(req, res) {
//   var newBook = new Book();

//   newBook.title = req.body.title;
//   newBook.author = req.body.author;
//   newBook.category = req.body.category;

//   newBook.save(function(err, book) {
//     if(err) {
//       res.send('error saving book');
//     } else {
//       console.log(book);
//       res.send(book);
//     }
//   });
// };

exports.addBooks = function(req, res){
  Book.create(req.body, function(err, book){
    if(err){
      res.send('Error saving book');
    } else {
      console.log(book);
      res.send(book);
    }
  });
};

exports.updateTitle = function(req, res) {
  Book.findOneAndUpdate({
    _id: req.params.id
    },
    { $set: { title: req.body.title }
  }, {upsert: true}, function(err, newBook) {
    if (err) {
      res.send('error updating ');
    } else {
      console.log(newBook);
      res.send(newBook);
    }
  });
};

exports.deleteBooks = function(req, res){
  Book.findOneAndRemove({
    _id: req.params.id
  }, function(err, book) {
    if(err) {
      res.send('Error removing')
    } else {
      console.log(book);
      res.status(204);
    }
  });
};
