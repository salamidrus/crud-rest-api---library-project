var express = require('express');
var router = express.Router();

var productController = require('../controllers/product');


router.get('/', productController.check);

router.get('/books', productController.showBooks);

router.get('/:id', productController.showBooksId);

router.post ('/add', productController.addBooks);

router.put ('/:id/updatetitle', productController.updateTitle);

router.delete('/:id/delete', productController.deleteBooks);



module.exports = router;